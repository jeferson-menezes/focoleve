import { storage } from '@/firebase'

export const StoreSaveImage = payload => new Promise((resolve, reject) => {
    let storageRef = storage.ref(`images/${payload.fileName}.jpg`)

    storageRef.put(payload.files).then(snapshot => {
        console.log("enviado " , snapshot);
    }).then(() => {
        storageRef.getDownloadURL().then(url => {
            return resolve(url)
        })
    }).catch(err => {
        return reject(err);
    })
})