import * as storage from '../storage'
import * as types from './mutation-types'
import { auth } from '@/firebase'


export const ActionDoLogin = ({ dispatch }, payload) => {
  if (auth.currentUser) {
    dispatch("ActionSignOut")
  }

  return auth.signInWithEmailAndPassword(payload.email, payload.password).then(res => {
    dispatch("ActionLoadSession", res)
  })
}

export const ActionCreateUser = ({ dispatch }, payload) => {
  return auth.createUserWithEmailAndPassword(payload.email, payload.password).then(res => {
    console.log("REs User ", res);
    dispatch("ActionDoLogin", payload)
  })
}
export const ActionCheckUID = ({ dispatch, state }) => {
  if (state.uid) {
    dispatch("ActionSetLogged", true)
    dispatch("ActionSetUID", uid)
    return Promise.resolve(state.uid)
  }

  const uid = storage.getIdSession()

  if (!uid) {
    return Promise.reject(new Error("UID inválido"))
  }
  
  dispatch("ActionSetLogged", true)
  dispatch("ActionSetUID", uid)
  return dispatch('ActionAuthStateChange')
}

export const ActionAuthStateChange = ({ dispatch }) => {
  return new Promise(async (resolve, reject) => {
    auth.onAuthStateChanged(user => {
      console.log("Change: ", user);

      if (user) {
        dispatch("ActionSetLogged", true)
        dispatch("ActionSetUID", user.uid)
        dispatch("ActionSetUser", user)
        resolve()
      } else {
        dispatch('ActionSignOut')
        reject("Sessao encerrada")
      }
    })
  })
}

export const ActionSignOut = ({ commit }) => {
  storage.setIdSession("")
  commit(types.SET_UID, "")
  commit(types.SET_LOGGED, false)
  return auth.signOut()
}

export const ActionLoadSession = ({ dispatch }, payload) => {
  dispatch("ActionSetLogged", true)
  dispatch("ActionSetUID", payload.user.uid)
  dispatch("ActionSetUser", payload.user)
  storage.setIdSession(payload.user.uid)
}

export const ActionPutUser = ({ dispatch }, payload) => {
  const user = auth.currentUser;
  if (!user) {
    dispatch("ActionSignOut")
    return
  }
  user.updateProfile({
    displayName: payload.nome,
    photoURL: payload.url
  }).then(res => {
    console.log("Res Get user ", res);
  }).catch(error => {
    console.log("Erro getUser", error);
  });

}

export const ActionSetLogged = ({ commit }, payload) => {
  commit(types.SET_LOGGED, payload)
}
export const ActionSetUID = ({ commit }, payload) => {
  commit(types.SET_UID, payload)
}
export const ActionSetUser = ({ commit }, payload) => {
  commit(types.SET_USER, payload)
}