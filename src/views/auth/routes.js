export default [
    {
      name: 'login',
      path: '/login',
      meta: { icon: 'fa-sign-in-alt', title: 'Login' },
      component: () => import(/* webpackChunkName: "login" */ './Login')
    }
  ]
  