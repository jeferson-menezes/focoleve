export const setIdSession = uid => localStorage.setItem('uid', uid)
export const getIdSession = () => localStorage.getItem('uid')