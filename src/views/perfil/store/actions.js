import * as types from './mutation-types'
import { firestore } from '@/firebase'

export const ActionSalvaPerfil = ({ dispatch }, payload) => {
    
    const ref = firestore.collection('perfil').doc(payload.uid)

    return ref.set(payload.form).then(res => {
        console.log(res);
        dispatch("ActionGetPerfil",payload.uid )
    })
}

export const ActionGetPerfil = ({ commit }, payload) => {
    const ref = firestore.collection('perfil').doc(payload)
    ref.get().then(res => {
        if (!res.exists) {
            console.log("Sem perfil");
        } else {
            console.log("Perfil ", res.data());
            commit(types.SET_PERFIL, res.data())
        }
    }).catch(err => {
        console.log("Erro ", err);
    })

}