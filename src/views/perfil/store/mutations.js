import * as types from './mutation-types'

export default {
  [types.SET_PERFIL](state, payload) {
    state.perfil = payload
  }
}