export default [
    {
      path: '/perfil',
      name: 'perfil',
      meta: { icon: 'fa-universal-access', title: 'Perfil', color:'green accent-4' },
      component: () => import(/* webpackChunkName: "home" */ './Perfil')
    }
  ]
  