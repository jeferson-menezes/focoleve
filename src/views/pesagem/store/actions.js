import * as types from './mutation-types'
import { firestore } from '@/firebase'

export const ActionSavePesagem = ({ dispatch }, payload) => {
    const ref = firestore.collection('pesagem').doc()
    return ref.set(payload).then(res => {
        console.log("Pesagem add", res);
        dispatch("ActionListPesagens", payload.userId)
    })
}

export const ActionListPesagens = ({ dispatch }, payload) => {
    console.log("UID: ",payload);
    
    firestore.collection('pesagem').where('userId', '==', payload).get()
        .then(res => {
            console.log(res);
            
            if (res.empty) {
                console.log("Não retornou nada");
                return
            }
            let lista = []
            res.forEach(doc => {
                lista.push(doc.data());
            });
            lista.sort((a, b) => {
                return a.data < b.data ? -1 : a.data > b.data ? 1 : 0
            })
            lista.reverse()
            dispatch("ActionSetPesagens", lista)
            dispatch("ActionSetLast", lista.slice(-1))
        }).catch(err => {
            console.log("Erro ao listar pesagens", err);
        })
}

export const ActionSetPesagens = ({ commit }, payload) => {
    commit(types.SET_PESAGENS, payload)
}

export const ActionSetLast = ({commit}, payload) => {
    commit(types.SET_LAST, payload)
}