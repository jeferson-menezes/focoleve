import * as types from './mutation-types'

export default {
      [types.SET_PESAGENS](state, payload) {
            state.pesagens = payload
      },
      [types.SET_LAST](state, payload) {
            state.last = payload
      }
}