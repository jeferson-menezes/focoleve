export default [
    {
      path: '/pesagem',
      name: 'pesagem',
      meta: { icon: 'fa-weight', title: 'Pesagem', color:'blue' },
      component: () => import(/* webpackChunkName: "home" */ './Pesagem')
    }
  ]
  