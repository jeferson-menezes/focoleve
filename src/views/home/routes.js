export default [
    {
      path: '/',
      name: 'home',
      meta: { icon: 'fa-heartbeat', title: 'Home', color:'red' },
      component: () => import(/* webpackChunkName: "home" */ './Home')
    }
  ]
  