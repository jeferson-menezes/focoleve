export const imc = (peso, altura) => {
    return (peso / (altura * altura)).toFixed(1)
}
export const fisico = imc => {
    if (imc < 18.5) {
        return 'Abaixo do peso'
    } else if (imc >= 18.5 && imc <= 24.9) {
        return 'Peso normal'
    } else if (imc >= 25 && imc <= 29.9) {
        return 'Sobrepeso'
    } else if (imc >= 30 && imc <= 34.9) {
        return 'Obesidade I'
    } else if (imc >= 35 && imc <= 39.9) {
        return 'Obesidade II'
    } else if (imc > 40) {
        return 'Obesidade III'
    }
}