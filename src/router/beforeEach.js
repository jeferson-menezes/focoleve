import store from '../store'
export default async (to, from, next) => {
  document.title = `${to.meta.title} - Foco Leve`
  if (to.name !== 'login' && !store.getters["auth/hasUID"]) {

    try {
      await store.dispatch('auth/ActionCheckUID')
      next({ name: to.name })
    } catch (err) {
      next({ name: 'login' })
    }
  } else {
    if (to.name === 'login' && store.getters["auth/hasUID"]) {
      next({ name: 'home' })
    } else {
      next()
    }
  }
}
