import { store as auth } from '@/views/auth'
import { store as pesagem } from '@/views/pesagem'
import { store as perfil } from '@/views/perfil'

export default {
    auth,
    pesagem,
    perfil
}